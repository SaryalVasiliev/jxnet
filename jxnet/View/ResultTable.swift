//
//  ResultTable.swift
//  jxnet
//
//  Created by iMac on 05.04.2020.
//  Copyright © 2020 Vasiliev S.I. All rights reserved.
//

import UIKit

class ResultTable {
    
    var controller: ExercisesViewController!
    
    init(_ viewController: UIViewController) {
        controller = (viewController as! ExercisesViewController)
    }
    
    func drawResultTable() {
        let label = UILabel()
        label.frame = CGRect.init(x: 0, y: 10, width: UIScreen.main.bounds.width, height: 50)
        var correctSum = 0
        for answer in incorrectAnswers {
            if answer == false {correctSum += 1}
        }
        label.text = "Результат: \(correctSum)/\(countQuestion!):"
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 27)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.minimumScaleFactor = 0.4
        self.view.addSubview(label)
        let label2 = UILabel()
        label2.frame = CGRect(x: 15, y: (self.navigationController?.navigationBar.frame.size.height)! + 60, width: (UIScreen.main.bounds.width / 2), height: 30)
        label2.text = "Уровень: 0"
        label2.textColor = .black
        label2.font = UIFont.systemFont(ofSize: 21)
        label2.adjustsFontSizeToFitWidth = true
        label2.textAlignment = .left
        label2.minimumScaleFactor = 0.4
        self.view.addSubview(label2)
        let label3 = UILabel()
        label3.frame = CGRect(x: (UIScreen.main.bounds.width / 2) + 10, y: (self.navigationController?.navigationBar.frame.size.height)! + 60, width: (UIScreen.main.bounds.width / 2) - 15, height: 30)
        label3.text = "+253"
        label3.textColor = .link
        label3.font = UIFont.systemFont(ofSize: 17)
        label3.adjustsFontSizeToFitWidth = true
        label3.textAlignment = .right
        label3.minimumScaleFactor = 0.4
        self.view.addSubview(label3)
        let progressLevel = UIProgressView()
        progressLevel.frame = CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)! + 110, width: UIScreen.main.bounds.width, height: 1)
        progressLevel.transform = progressLevel.transform.scaledBy(x: 1, y: 20)
        progressLevel.progress = 0.75
        self.view.addSubview(progressLevel)
        resultTable.delegate = self
        resultTable.dataSource = self
        resultTable.register(BundleCell.self, forCellReuseIdentifier: "cell")
        resultTable.frame = CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)! + 110, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (self.tabBarController?.tabBar.frame.height)! - (self.navigationController?.navigationBar.frame.height)! - 164)
        view.addSubview(resultTable)
        repeatButton = UIButton(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - (self.tabBarController?.tabBar.frame.height)! - 54, width: UIScreen.main.bounds.width, height: 54))
        repeatButton.setTitle("Попробовать еще раз", for: .normal)
        repeatButton.setTitleColor(.white, for: .normal)
        repeatButton.backgroundColor = UIColor.init(hexFromString: "#3333CC")
        repeatButton.addTarget(nil, action: #selector(repeatAction), for: .touchUpInside)
        resultTable.reloadData()
        view.addSubview(repeatButton)
    }
}
