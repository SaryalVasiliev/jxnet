//
//  HandWritingViewController.swift
//  jxnet
//
//  Created by iMac on 05.04.2020.
//  Copyright © 2020 Vasiliev S.I. All rights reserved.
//

import UIKit

class HandWritingViewController: UIViewController {
    
    @IBOutlet weak var resultCollectionView: UICollectionView!
    @IBOutlet weak var drawableView: DrawableView!
    @IBOutlet weak var showResult: UITextView!
    
    private lazy var viewModel: HandwritingViewModel = {
        let vm = HandwritingViewModel(canvas: drawableView)
        return vm
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultCollectionView.delegate = self
        resultCollectionView.dataSource = self
        resultCollectionView.layer.borderWidth = 1
        resultCollectionView.layer.borderColor = UIColor.blue.cgColor
        
        drawableView.delegate = self
        
        viewModel.resultUpdated = { [weak self]() in
            self?.resultCollectionView.reloadData()
        }
    }
    
    @IBAction func handleClearAction() {
        drawableView.clear()
        viewModel.clear()
    }
    
    @IBAction func handleUndoAction() {
        drawableView.undo()
        viewModel.clear()
        
        let strokes = drawableView.strokes.map { $0 as! [NSValue] }
        if strokes.count > 0 {
            viewModel.add(strokes)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//--------------------------------------------------------------------------------
//  MARK: - UICollectionView delegate
//--------------------------------------------------------------------------------

extension HandWritingViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.resultCount()
    }
    
    func collectionView(_ view: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = view.dequeueReusableCell(withReuseIdentifier: "viewCell", for: indexPath) as! HandwritingCollectionViewCell
        cell.resultLabel.text = viewModel.result(atIndex: indexPath.item)
        return cell
    }
}

//--------------------------------------------------------------------------------
//  MARK: - DrawableView delegate
//--------------------------------------------------------------------------------

extension HandWritingViewController: DrawableViewDelegate {
    
    func didDraw(stroke: [NSValue]) {
        viewModel.add(stroke)
    }
}
