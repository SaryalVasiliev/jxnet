//
//  StandartQuestionLabel.swift
//  jxnet
//
//  Created by iMac on 05.04.2020.
//  Copyright © 2020 Vasiliev S.I. All rights reserved.
//

import UIKit

class StandartQuestionLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        font = .systemFont(ofSize: 100)
        adjustsFontSizeToFitWidth = true
        minimumScaleFactor = 0.4
        //showAsk1.backgroundColor = .
        textAlignment = .center
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        //super.init(coder: coder)
        //setFont()
    }
    
    
    //Вот это пока не понял
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        font = .systemFont(ofSize: 100)
        adjustsFontSizeToFitWidth = true
        minimumScaleFactor = 0.4
        //showAsk1.backgroundColor = .
        textAlignment = .center
    }
 */
}
