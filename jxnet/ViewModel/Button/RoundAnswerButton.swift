//
//  RoundAnswerButton.swift
//  jxnet
//
//  Created by iMac on 05.04.2020.
//  Copyright © 2020 Vasiliev S.I. All rights reserved.
//

import UIKit

class RoundAnswerButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.titleLabel?.font = .systemFont(ofSize: 35.0)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.minimumScaleFactor = 0.4
        self.layer.cornerRadius = 50.0
        self.layer.borderWidth = 1.0
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
