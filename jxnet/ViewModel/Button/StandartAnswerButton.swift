//
//  StandartAnswerButton.swift
//  jxnet
//
//  Created by iMac on 05.04.2020.
//  Copyright © 2020 Vasiliev S.I. All rights reserved.
//

import UIKit

class StandartAnswerButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = frame.height / 4
        clipsToBounds = true
        layer.masksToBounds = false
        layer.shadowRadius = 5
        layer.shadowOpacity = 1.0
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowColor = UIColor.init(hexFromString: "#CCCCCC").cgColor
        backgroundColor = .white
        setTitleColor(.black, for: .normal)
        self.titleLabel?.font = .systemFont(ofSize: 30.0)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.minimumScaleFactor = 0.4
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //Вот это пока не понял
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        layer.cornerRadius = frame.height / 4
        clipsToBounds = true
        layer.masksToBounds = false
        layer.shadowRadius = 5
        layer.shadowOpacity = 1.0
        layer.shadowOffset = CGSize(width: 1, height: 1)
        //layer.shadowColor = UIColor.init(hexFromString: "#CCCCCC").cgColor
        backgroundColor = .white
        setTitleColor(.black, for: .normal)
        self.titleLabel?.font = .systemFont(ofSize: 30.0)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.minimumScaleFactor = 0.4
    }
 */
    

}
